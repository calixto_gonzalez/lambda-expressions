import person.Person;

import java.util.Comparator;
import java.util.function.Function;

public class LambdaPOC {
    public static void main(String[] args) {
        Person person1 = new Person();
        person1.setLastName("Escobar");
        person1.setName("Pablo");
        person1.setAge(59);
        Person person2 = new Person();
        person2.setLastName("Escobar");
        person2.setName("Pablo");
        person2.setAge(65);

        Comparator<Person> compareAge = (p1, p2) -> p2.getAge() - p1.getAge();

        Comparator<Person> compareName = (p1, p2) -> p1.getName().compareTo(p2.getName());

        Comparator<Person> compareLastName = (p1, p2) -> p1.getLastName().compareTo(p2.getLastName());


        Function<Person, Integer> f1 = person -> person.getAge();

        Function<Person, String> f2 = person -> person.getName();

        Function<Person, String> f3 = person -> person.getLastName();

        Comparator<Person> comparePersonAge = Comparator.comparing(person -> person.getAge());

        Comparator<Person> comparePersonName = Comparator.comparing(f2);

        Comparator<Person> comparePersonLastName = Comparator.comparing(Person::getLastName);
        Comparator<Person> cmp = Comparator.comparing(Person::getLastName)
                .thenComparing(Person::getName)
                .thenComparing(Person::getAge);


        System.out.println("Compare just name:  "+compareName.compare(person1,person2));

        System.out.println("Compare just last name:  "+comparePersonLastName.compare(person1,person2));

        System.out.println("Compare name,lastname and age:  "+cmp.compare(person1,person2));

    }
}



